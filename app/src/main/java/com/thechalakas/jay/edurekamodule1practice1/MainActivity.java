package com.thechalakas.jay.edurekamodule1practice1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button button_1 = (Button) findViewById(R.id.button1);

        button_1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("Lifecycle","---------------------------------View.OnClickListener reached ");
            }
        });

        button_1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view)
            {

                Log.i("Lifecycle","---------------------------------View.OnLongClickListener reached ");
                return false;
            }
        });
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("Lifecycle","---------------------------------onStart reached ");
    }



}
